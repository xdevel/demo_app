<?php namespace Users\JsonRPC;

use Users\JsonRPC\Errors\InvalidRequest;
use Users\JsonRPC\Response\Error;
use Users\JsonRPC\Response\Success;

class App extends \Phalcon\Mvc\Micro
{
    protected $responsePayload = [];
    protected $routes = [];

    public function handle($uri = null)
    {
        $payload = $this->request->getJsonRawBody();
        if (empty($payload)) {
            throw new InvalidRequest;
        }
        $payload = is_object($payload) ? [$payload] : $payload;

        foreach ($payload as $route) {
            try {
                $this->jrpcRequest->fromRequest($route);

                $handledRequest = parent::handle($this->jrpcRequest->getRoute());

                if ($handledRequest !== null) {
                    $this->responsePayload[] = $handledRequest->toArray();
                }
            } catch (\Exception $e) {
                $this->responsePayload[] = (new Error($route->id ?? null, [], $e))->toArray();
            }
        }

        $this->responsePayload = count($this->responsePayload) === 1 ? $this->responsePayload[0] : $this->responsePayload;

        $this->response->setJsonContent($this->responsePayload);
        $this->response->send();
    }

    public function getCurrentRequest()
    {
        return $this->jrpcRequest;
    }
}
