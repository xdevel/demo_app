<?php namespace Users\JsonRPC\Response;


class Success extends Response
{
    protected $result = null;

    public function __construct($id, array $payload)
    {
        $this->id     = $id;
        $this->result = $payload;
    }

    public function toArray(): array
    {
        return [
            'jsonrpc' => '2.0',
            'id'      => $this->id,
            'result'  => $this->result
        ];
    }
}
