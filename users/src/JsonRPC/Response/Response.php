<?php namespace Users\JsonRPC\Response;


abstract class Response
{
    protected $error = [
        'code' => 0,
        'message' => '',
    ];
    protected $id = null;
    protected $jsonrpc = "2.0";

    abstract public function toArray() : array;

    public function toJson()
    {
        return json_encode($this->toArray());
    }
}
