<?php namespace Users\JsonRPC\Response;


class Error extends Response
{
    protected $error = [
        'code'    => 0,
        'message' => '',
    ];

    public function __construct($id, array $data = [], \Exception $e = null)
    {
        $this->id = $id;

        if ($e !== null) {
            $this->withException($e);
        }

        if (!empty($data)) {
            $this->error['data'] = $data;
        }
    }

    public function toArray(): array
    {
        return [
            'jsonrpc' => '2.0',
            'id'      => $this->id,
            'error'   => $this->error
        ];
    }

    public function withException(\Exception $e)
    {
        $this->error['code']    = $e->getCode();
        $this->error['message'] = $e->getMessage();
    }

    public function withError(array $error)
    {
        $this->error['code']    = $error['code'] ?? -32603;
        $this->error['message'] = $error['message'] ?? 'Internal Error';
    }
}
