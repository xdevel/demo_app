<?php


namespace Users\JsonRPC;


use Phalcon\Validation;
use Users\JsonRPC\Errors\InvalidRequest;

class Request
{
    public $id = null;
    public $jsonrpc = "2.0";
    public $method;
    public $params;

    /**
     * Fill from stdObject request data
     *
     * @param $request
     *
     * @throws \Users\JsonRPC\Errors\InvalidRequest
     */
    public function fromRequest($request)
    {
        $this->clear();
        $this->validate($request);

        $this->id     = $request->id ?? null;
        $this->method = $request->method;
        $this->params = $request->params ?? [];
    }

    /** Clear request data */
    public function clear()
    {
        $this->id      = null;
        $this->jsonrpc = "2.0";
        $this->method  = '';
        $this->params  = [];
    }

    /**
     * Performs basic request validation
     *
     * @param $request
     *
     * @throws \Users\JsonRPC\Errors\InvalidRequest
     */
    public function validate($request)
    {
        if (!is_object($request)) {
            throw new InvalidRequest;
        }

        if (!property_exists($request, 'jsonrpc') || $request->jsonrpc !== "2.0") {
            throw new InvalidRequest;
        }

        if (!property_exists($request, 'method') || count(explode('.', $request->method)) !== 2) {
            throw new InvalidRequest;
        }
    }

    /**
     * Do we need to return response? (is it notification request)
     *
     * @return bool
     */
    public function isNotification()
    {
        return $this->id === null;
    }

    /**
     * Validate id of request
     *
     * @return bool
     */
    public function validateId()
    {
        $validation = new Validation;
        $validation->add('id', new Validation\Validator\PresenceOf());
        $validation->add('id', new Validation\Validator\Callback([
            "message"  => "Value should be string or numeric",
            "callback" => function ($data) {
                if (!is_string($data->id) && !is_numeric($data->id)) {
                    return false;
                }

                return true;
            }
        ]));
        $messages = $validation->validate($this);

        return $messages->count() === 0;
    }

    /**
     * Get url for router
     *
     * @return string
     */
    public function getRoute()
    {
        list($controller, $action) = explode('.', $this->method);

        return "/$controller/$action";
    }
}
