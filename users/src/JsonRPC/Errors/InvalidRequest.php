<?php namespace Users\JsonRPC\Errors;


class InvalidRequest extends Error
{
    protected $code = -32600;
    protected $message = 'Invalid Request';
}
