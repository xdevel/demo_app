<?php


namespace Users\JsonRPC\Errors;


class ParseError extends Error
{
    protected $code = -32700;
    protected $message = 'Parse error';
}
