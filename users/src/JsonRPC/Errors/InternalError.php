<?php


namespace Users\JsonRPC\Errors;


class InternalError extends Error
{
    protected $code = -32603;
    protected $message = 'Internal Error';
}
