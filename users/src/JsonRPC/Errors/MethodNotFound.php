<?php


namespace Users\JsonRPC\Errors;


class MethodNotFound extends Error
{
    protected $code = -32601;
    protected $message = 'Method not found';
}
