<?php


namespace Users\JsonRPC\Errors;


class InvalidParams extends Error
{
    protected $code = -32602;
    protected $message = 'Invalid params';
}
