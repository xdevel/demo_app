<?php namespace Users\JsonRPC\Errors;


use Throwable;

class Error extends \Phalcon\Exception
{
    public function __construct($message = '', $code = 0, Throwable $previous = null) { }

}
