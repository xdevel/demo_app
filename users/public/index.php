<?php

use Users\JsonRPC\App;

error_reporting(E_ALL);

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

try {
    /**
     * The FactoryDefault Dependency Injector automatically registers the services that
     * provide a full stack framework. These default services can be overidden with custom ones.
     */
    $di = new \Phalcon\Di();

    $config = include APP_PATH . '/config/config.php';

    /** Include Autoloader */
    include APP_PATH . '/config/loader.php';

    /**
     * Include Services
     */
    include APP_PATH . '/config/services.php';

    /**
     * Get config service for use in inline setup below
     */
    $config = $di->getConfig();


    /**
     * Starting the application
     * Assign service locator to the application
     */
    $app = new App($di);

    /**
     * Include Application
     */
    include APP_PATH . '/app.php';

    /**
     * Handle the request
     */
    $app->handle();

} catch (\Users\JsonRPC\Errors\Error $e) {
    $app->response->setJsonContent((new \Users\JsonRPC\Response\Error($app->getCurrentRequest()->id ?? null, [], $e))->toArray());
    $app->response->send();
} catch (\Exception $e) {
    $app->response->setJsonContent((new \Users\JsonRPC\Response\Error(null, ['message' => $e->getMessage()], new \Users\JsonRPC\Errors\InternalError()))->toArray());
    $app->response->send();
}
