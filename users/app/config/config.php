<?php

return new \Phalcon\Config([
    'application' => [
        'modelsDir'      => __DIR__ . '/../models/',
        'controllersDir' => __DIR__ . '/../controllers/',
        'validationsDir'  => __DIR__ . '/../validations/',
        'migrationsDir'  => __DIR__ . '/../migrations/',
        'baseUri'        => '/',
    ],
    'database'    => [
        'adapter'  => 'Sqlite',
        'username' => null,
        'password' => null,
        'host'     => null,
        'charset'  => 'UTF-8',
        'dbname'   => '/var/www/html/db/users.sqlite'
    ]
]);
