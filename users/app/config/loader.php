<?php

/**
 * Registering an autoloader
 */
$loader = new \Phalcon\Loader();

$loader->registerDirs(
    [
        $config->application->modelsDir,
        $config->application->controllersDir,
        $config->application->validationsDir,
    ]
)->register();

require_once __DIR__ . '/../../vendor/autoload.php';
