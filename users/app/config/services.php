<?php

$di->setShared('config', function () use ($config) { return $config; });
$di->setShared('request', function () { return new \Phalcon\Http\Request(); });
$di->setShared('response', function () { return new \Phalcon\Http\Response(); });
$di->setShared('modelsManager', function () { return new \Phalcon\Mvc\Model\Manager(); });
$di->setShared('modelsMetadata', function () { return new \Phalcon\Mvc\Model\MetaData\Memory(); });
$di->setShared('jrpcRequest', function () { return new \Users\JsonRPC\Request(); });
$di->setShared('router', function () { return new \Phalcon\Mvc\Router(); });
$di->setShared('security', function () { return new \Phalcon\Security(); });

$di->setShared('db', function () {
    $config = $this->getConfig();

    $class  = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
    $params = [
        'host'     => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname'   => $config->database->dbname,
        'charset'  => $config->database->charset
    ];

    if ($config->database->adapter == 'Postgresql') {
        unset($params['charset']);
    }

    $connection = new $class($params);

    return $connection;
});
