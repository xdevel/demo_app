<?php

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Mvc\Model\Migration;

class CreateUsersMigration_100 extends Migration
{
    public function up()
    {
        $this->morphTable(
            'users',
            [
                'columns' => [
                    new Column(
                        'id',
                        [
                            'type'          => Column::TYPE_INTEGER,
                            'notNull'       => true,
                            'autoIncrement' => true,
                            'size'          => 10,
                            'first'         => true
                        ]
                    ),
                    new Column(
                        'login',
                        [
                            'type'  => Column::TYPE_VARCHAR,
                            'size'  => 45,
                            'after' => 'id'
                        ]
                    ),
                    new Column(
                        'password',
                        [
                            'type'  => Column::TYPE_VARCHAR,
                            'size'  => 255,
                            'after' => 'login'
                        ]
                    ),
                ],
                'indexes' => [
                    new Index('PRIMARY', ['id']),
                    new Index('id_UNIQUE', ['id']),
                    new Index('lg_UNIQUE', ['login']),
                ]
            ]
        );

        self::$connection->insert('users', ['admin', (new Phalcon\Security)->hash('admin')], ['login', 'password']);
    }
}
