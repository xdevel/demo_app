<?php
/**
 * Local variables
 *
 * @var \Phalcon\Mvc\Micro $app
 */

$auth = new \Phalcon\Mvc\Micro\Collection();
$auth->setHandler(AuthController::class, true);
$auth->setPrefix('/auth');
$auth->map('/login', 'login');
$app->mount($auth);

/**
 * Not found handler
 */
$app->notFound(function () use ($app) {
    $route = $app->getCurrentRequest();
    if (!isset($route->id)) {
        return null;
    }
    throw new \Users\JsonRPC\Errors\MethodNotFound;
});
