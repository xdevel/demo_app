<?php

use Users\JsonRPC\Response\Success;

class AuthController extends \Phalcon\Di\Injectable
{
    public function login()
    {
        $params = $this->jrpcRequest->params;

        $validator = new LoginValidation();
        $messages  = $validator->validate($params);

        if (!$this->jrpcRequest->validateId()) {
            throw new \Users\JsonRPC\Errors\InvalidRequest();
        }

        if ($messages->count() > 0) {
            throw new \Users\JsonRPC\Errors\InvalidParams();
        }

        $user = Users::findFirst(['conditions' => 'login=:login:', 'bind' => ['login' => $params->login,]]);

        return new Success($this->jrpcRequest->id, [
            'success' => !empty($user) && $this->security->checkHash($params->password, $user->password),
        ]);
    }
}
