<?php


class LoginValidation extends \Phalcon\Validation
{
    public function initialize()
    {
        $this->add(
            'login',
            new \Phalcon\Validation\Validator\PresenceOf()
        );
        $this->add(
            'login',
            new \Phalcon\Validation\Validator\Alnum()
        );
        $this->add(
            'password',
            new \Phalcon\Validation\Validator\PresenceOf()
        );
    }
}
