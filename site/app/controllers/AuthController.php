<?php


/**
 * @property \Graze\GuzzleHttp\JsonRpc\Client rpc
 */
class AuthController extends \Phalcon\Di\Injectable
{
    public function login()
    {
        if (!$this->security->checkToken()) {
            $this->flashSession->error('Token mismatch.');

            return $this->response->redirect('/')->send();
        }

        $validation = new LoginValidation();
        $messages   = $validation->validate($this->request->getPost());

        if ($messages->count() > 0) {
            foreach ($messages as $message) {
                $this->flashSession->error($message->getMessage());
            }

            return $this->response->redirect('/')->send();
        }

        $request = $this->rpc->request(
            $this->security->getRandom()->uuid(),
            'auth.login', [
            'login'    => $validation->getValue('login'),
            'password' => $validation->getValue('password'),
        ]);

        try {
            $response = $this->rpc->send($request);

            if (!empty($response->getRpcResult()['success'])) {
                $this->flashSession->success('успешная авторизация');

                return $this->response->redirect('/')->send();
            }

        } catch (\Exception $e) {
        }

        $this->flashSession->error('неверный логин или пароль');

        return $this->response->redirect('/')->send();
    }
}
