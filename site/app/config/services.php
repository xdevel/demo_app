<?php

use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View\Simple as View;
use Phalcon\Session\Adapter\Files as Session;

$di->setShared('config', function () use ($config) { return $config; });

/**
 * Sets the view component
 */
$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new View();
    $view->setViewsDir($config->application->viewsDir);

    return $view;
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

/**
 * Register router
 */
$di->setShared('router', function () {
    $router = new \Phalcon\Mvc\Router();
    $router->setUriSource(
        \Phalcon\Mvc\Router::URI_SOURCE_SERVER_REQUEST_URI
    );

    return $router;
});

$di->setShared('session', function () {
    $session = new Session();
    $session->start();

    return $session;
});

$di->setShared('rpc', function () use($config) {
    $client = \Graze\GuzzleHttp\JsonRpc\Client::factory($config->rpc->server);

    return $client;
});
