<?php

return new \Phalcon\Config([
    'application' => [
        'modelsDir'      => __DIR__ . '/../models/',
        'controllersDir' => __DIR__ . '/../controllers/',
        'validationsDir' => __DIR__ . '/../validations/',
        'viewsDir'       => __DIR__ . '/../views/',
        'baseUri'        => '/',
    ],
    'rpc'         => [
        'server' => 'http://users/'
    ]
]);
