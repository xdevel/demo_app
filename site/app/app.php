<?php
/**
 * Local variables
 *
 * @var \Phalcon\Mvc\Micro $app
 */

$app->get('/', function () {
    echo $this->view->render('index');
});

$auth = new \Phalcon\Mvc\Micro\Collection();
$auth->setHandler(AuthController::class, true);
$auth->setPrefix('/auth');
$auth->post('/login', 'login');
$app->mount($auth);

$app->notFound(function () use ($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo $app['view']->render('404');
});
