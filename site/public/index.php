<?php

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Micro;

error_reporting(E_ALL);

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

try {
    $di     = new FactoryDefault();
    $config = include APP_PATH . '/config/config.php';

    include APP_PATH . '/config/loader.php';
    include APP_PATH . '/config/services.php';

    $app = new Micro($di);

    include APP_PATH . '/app.php';

    $app->handle();

} catch (\Exception $e) {
    echo $e->getMessage() . '<br>';
    echo '<pre>' . $e->getTraceAsString() . '</pre>';
}
