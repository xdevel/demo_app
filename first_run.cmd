cd "%~p1"
docker-compose build
docker-compose up -d
docker-compose exec site_fpm composer update
docker-compose exec users_fpm composer update
docker-compose exec users_fpm /root/.composer/vendor/bin/phalcon migration run
